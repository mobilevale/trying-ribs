//
//  AuthenticationBuilder.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 14/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs


public protocol AuthenticationDependency: Dependency {
    
    var authenticationViewController: AuthenticationViewControllable { get }

    var appDelegate: AppDelegate {get}
}

open class AuthenticationComponent: Component<AuthenticationDependency> {

    public var appDelegate: AppDelegate {
        return dependency.appDelegate
    }
    
    // TODO: Make sure to convert the variable into lower-camelcase.
    // TODO: Declare 'fileprivate' dependencies that are only used by this RIB.
    public var authenticationViewController: AuthenticationViewControllable {
        return dependency.authenticationViewController
    }
}

// MARK: - Builder

public protocol AuthenticationBuildable: Buildable {
    func build(
        withListener listener: AuthenticationListener
    ) -> AuthenticationRouting
}

open class AuthenticationBuilder: Builder<AuthenticationDependency>, AuthenticationBuildable {

    public override init(dependency: AuthenticationDependency) {
        super.init(dependency: dependency)
    }

    open func build(
        withListener listener: AuthenticationListener
    ) -> AuthenticationRouting {
        let component = AuthenticationComponent(
            dependency: dependency
        )
        let interactor = AuthenticationInteractor()
        interactor.listener = listener
        
        let signInBuilder = SignInBuilder(dependency: component)

        return AuthenticationRouter(
            interactor: interactor,
            viewController: component.authenticationViewController,
            signInBuilder: signInBuilder
        )
    }
}
