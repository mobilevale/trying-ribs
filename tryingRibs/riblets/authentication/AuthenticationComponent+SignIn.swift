import Foundation
import RIBs


protocol AuthenticationDependencySignIn: Dependency {
}

extension AuthenticationComponent: SignInDependency {
}
