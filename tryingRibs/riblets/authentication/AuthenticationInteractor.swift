//
//  AuthenticationInteractor.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 14/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs
import RxSwift

public protocol AuthenticationRouting: Routing {
    func cleanupViews()
    // TODO: Declare methods the interactor can invoke to manage sub-tree via the router.
    func routeToSignIn()
    func detach(completion: (() -> Void)?)
}

public protocol AuthenticationListener: class {
    // TODO: Declare methods the interactor can invoke to communicate with other RIBs.
    func closeAuthentication()
}

open class AuthenticationInteractor: Interactor, AuthenticationInteractable {
    public weak var router: AuthenticationRouting?
    public weak var listener: AuthenticationListener?

    
    
    // TODO: Add additional dependencies to constructor. Do not perform any logic
    // in constructor.
    public override init() {
        super.init()
    }

    open override func didBecomeActive() {
        super.didBecomeActive()
        // TODO: Implement business logic here.
    }

    open override func willResignActive() {
        super.willResignActive()

        router?.cleanupViews()
        // TODO: Pause any business logic.
    }
    
    open func closeSignIn() {
        self.router?.detach {
            self.listener?.closeAuthentication()
        }
    }
}
