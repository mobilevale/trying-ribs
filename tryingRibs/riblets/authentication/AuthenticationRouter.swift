//
//  AuthenticationRouter.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 14/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs

public protocol AuthenticationInteractable:
    Interactable,
    SignInListener
{
    weak var router: AuthenticationRouting? { get set }
    weak var listener: AuthenticationListener? { get set }
}

public protocol AuthenticationViewControllable: ViewControllable {
    // TODO: Declare methods the router invokes to manipulate the view hierarchy. Since
    // this RIB does not own its own view, this protocol is conformed to by one of this
    // RIB's ancestor RIBs' view.
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable, completion: (() -> Void)?)
}

open class AuthenticationRouter: ViewableRouter<AuthenticationInteractable, AuthenticationViewControllable>, AuthenticationRouting {
    
    public var currentChild: Routing?
    
    private let signInBuilder: SignInBuildable
    private var signIn: SignInRouting?
    
    // TODO: Constructor inject child builder protocols to allow building children.
    
    public init(
        interactor: AuthenticationInteractable,
        viewController: AuthenticationViewControllable,
        signInBuilder: SignInBuildable
    ) {
        self.signInBuilder = signInBuilder
        super.init(
            interactor: interactor,
            viewController: viewController
        )
        interactor.router = self
    }

    open func cleanupViews() {
        // TODO: Since this router does not own its view, it needs to cleanup the views
        // it may have added to the view hierarchy, when its interactor is deactivated.
        
//        if let currentViewableRoutingChild = self.currentChild as? ViewableRouting {
//            self.viewController.dismiss(
//                viewController: currentViewableRoutingChild.viewControllable,
//                completion: nil
//            )
//        }
    }
    
    private func detachCurrentChild(completion: (()->Void)?) {
        if let currentRoutingChild = self.currentChild {
            self.detachChild(currentRoutingChild)
            if let currentViewableRoutingChild = currentRoutingChild as? ViewableRouting {
                self.viewController.dismiss(
                    viewController: currentViewableRoutingChild.viewControllable,
                    completion: completion
                )
            }
            else {
                completion?()
            }
        }
        else {
            completion?()
        }
    }
    
    public func detach(completion: (() -> Void)?) {
        self.detachCurrentChild(completion: completion)
    }
    
    open override func didLoad() {
        super.didLoad()
        self.routeToSignIn()
    }
    
    open func routeToSignIn() {
        self.detachCurrentChild {
            self.signIn = self.signInBuilder.build(
                withListener: self.interactor
            )
            self.attachChild(self.signIn!)
            self.currentChild = self.signIn!
            self.viewController.present(
                viewController: self.signIn!.viewControllable
            )
        }
    }
}
