//
//  HomeComponent+Authentication.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 14/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import Foundation
import RIBs


protocol HomeDependencyAuthentication: Dependency {
}

extension HomeComponent: AuthenticationDependency {
    var authenticationViewController: AuthenticationViewControllable {
        return homeViewController
    }
}

