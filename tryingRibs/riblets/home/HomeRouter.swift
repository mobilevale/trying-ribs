//
//  HomeRouter.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs


public protocol HomeInteractable: Interactable, AuthenticationListener {
    weak var router: HomeRouting? { get set }
    weak var listener: HomeListener? { get set }
}

public protocol HomeViewControllable: ViewControllable {
    // TODO: Declare methods the router invokes to manipulate the view hierarchy.
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable, completion: (()->Void)?)
}

open class HomeRouter: ViewableRouter<HomeInteractable, HomeViewControllable>, HomeRouting {

    public var currentChild: Routing?
    private let authenticationBuilder: AuthenticationBuildable
    private var authentication: AuthenticationRouting?

    // TODO: Constructor inject child builder protocols to allow building children.
    public init(
        interactor: HomeInteractable,
        viewController: HomeViewControllable,
        authenticationBuilder: AuthenticationBuildable
    ) {
        self.authenticationBuilder = authenticationBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    private func detachCurrentChild(completion: (()->Void)?) {
        if let currentRoutingChild = self.currentChild {
            self.detachChild(currentRoutingChild)
            if let currentViewableRoutingChild = currentRoutingChild as? ViewableRouting {
                self.viewController.dismiss(
                    viewController: currentViewableRoutingChild.viewControllable,
                    completion: completion
                )
            }
            else {
                completion?()
            }
        }
        else {
            completion?()
        }
    }
    
    public func detach(completion: (() -> Void)?) {
        self.detachCurrentChild(
            completion: completion
        )
    }
    
    open func routeToAuthentication() {
        self.detachCurrentChild() {
            self.authentication = self.authenticationBuilder.build(
                withListener: self.interactor
            )
            self.attachChild(self.authentication!)
            self.currentChild = self.authentication
        }
    }
}
