//
//  HomeViewController.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs
import RxSwift
import UIKit



public protocol HomePresentableListener: class {
    // TODO: Declare properties and methods that the view controller can invoke to perform
    // business logic, such as signIn(). This protocol is implemented by the corresponding
    // interactor class.
    func didSignInTap()
}

open class HomeViewController: UIViewController, HomePresentable, HomeViewControllable {
    
    
    /// The UIKit view representation of this view.
    public final var uiviewController: UIViewController { return self }
    public weak var listener: HomePresentableListener?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction open func didSignInTap(_ sender: AnyObject) {
        self.listener?.didSignInTap()
    }
    
    public func present(viewController: ViewControllable) {
        self.present(
            viewController.uiviewController,
            animated: true,
            completion: nil
        )
    }
    
    open func dismiss(viewController: ViewControllable, completion: (() -> Void)?) {
        self.dismiss(
            animated: true,
            completion: completion
        )
    }
}

extension HomeViewController : AuthenticationViewControllable {
}
