//
//  Copyright (c) 2017. Uber Technologies
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RIBs


protocol RootDependency: Dependency {
    // TODO: Declare the set of dependencies required by this RIB, but cannot be
    // created by this RIB.
    
}

final class RootComponent: Component<RootDependency> {
    let application: UIApplication
    let launchOptions: [UIApplicationLaunchOptionsKey : Any]?
    let appDelegate: AppDelegate
    
    init(
        dependency: RootDependency,
        application: UIApplication,
        launchOptions: [UIApplicationLaunchOptionsKey : Any]?,
        appDelegate: AppDelegate
    ) {
        self.application = application
        self.launchOptions = launchOptions
        self.appDelegate = appDelegate
        super.init(dependency: dependency)
    }
}

// MARK: - Builder

protocol RootBuildable: Buildable {
    func build(
        application: UIApplication,
        launchOptions: [UIApplicationLaunchOptionsKey : Any]?,
        appDelegate: AppDelegate
    ) -> LaunchRouting
}

final class RootBuilder: Builder<RootDependency>, RootBuildable {

    override init(dependency: RootDependency) {
        super.init(dependency: dependency)
    }

    func build(
        application: UIApplication,
        launchOptions: [UIApplicationLaunchOptionsKey : Any]?,
        appDelegate: AppDelegate
    ) -> LaunchRouting {
        let component = RootComponent(
            dependency: dependency,
            application: application,
            launchOptions: launchOptions,
            appDelegate: appDelegate
        )
        
        let viewController = RootViewController()
        viewController.appDelegate = appDelegate
        
        let interactor = RootInteractor(presenter: viewController)

        let homeBuilder = HomeBuilder(
            dependency: component
        )
        
        return RootRouter(
            interactor: interactor,
            viewController: viewController,
            homeBuilder: homeBuilder
        )
    }
}
