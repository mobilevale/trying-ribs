//
//  Copyright (c) 2017. Uber Technologies
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RIBs


protocol RootInteractable: Interactable, HomeListener {
    weak var router: RootRouting? { get set }
    weak var listener: RootListener? { get set }
}

protocol RootViewControllable: ViewControllable {
    func setRootViewController(viewController: ViewControllable)
}

final class RootRouter: LaunchRouter<RootInteractable, RootViewControllable>, RootRouting {
    public var currentChild: Routing?

    private let homeBuilder: HomeBuildable
    private var home: ViewableRouting?
    
    init(interactor: RootInteractable,
         viewController: RootViewControllable,
         homeBuilder: HomeBuildable
        ) {
        self.homeBuilder = homeBuilder
        
        super.init(
            interactor: interactor,
            viewController: viewController
        )
        interactor.router = self
    }
    
    override func didLoad() {
        super.didLoad()
        self.routeToHome()
    }
    
    private func detachCurrentChild(completion: (()->Void)?) {
        if let currentRoutingChild = self.currentChild {
            self.detachChild(currentRoutingChild)
        }
        completion?()
    }
    
    func routeToHome() {
        self.detachCurrentChild() {
            self.home = self.homeBuilder.build(withListener: self.interactor)
            self.attachChild(self.home!)
            self.currentChild = self.home
            
            self.viewController.setRootViewController(
                viewController: self.home!.viewControllable
            )
        }
    }
}
