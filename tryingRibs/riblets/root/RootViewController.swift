//
//  Copyright (c) 2017. Uber Technologies
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import RIBs
import UIKit


public protocol RootPresentableListener: class {
    // TODO: Declare properties and methods that the view controller can invoke to perform
    // business logic, such as signIn(). This protocol is implemented by the corresponding
    // interactor class.
}

open class RootViewController: UIViewController, RootPresentable, RootViewControllable {
    
    weak public var listener: RootPresentableListener?

    public var appDelegate: AppDelegate!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("Method is not supported")
    }

    open override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
    }

    // MARK: - RootViewControllable
    public func setRootViewController(viewController: ViewControllable) {
        let window: UIWindow = (UIApplication.shared.delegate?.window)! ?? UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = viewController.uiviewController
        window.makeKeyAndVisible()
    }
}
