//
//  SignInBuilder.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs


public protocol SignInDependency: Dependency {
    // TODO: Declare the set of dependencies required by this RIB, but cannot be
    // created by this RIB.
}

open class SignInComponent: Component<SignInDependency> {

    // TODO: Declare 'fileprivate' dependencies that are only used by this RIB.
    let signInViewController: SignInViewController
    
    init(dependency: SignInDependency, signInViewController: SignInViewController) {
        self.signInViewController = signInViewController
        super.init(dependency: dependency)
    }
}

// MARK: - Builder

public protocol SignInBuildable: Buildable {
    func build(
        withListener listener: SignInListener
    ) -> SignInRouting
}

open class SignInBuilder: Builder<SignInDependency>, SignInBuildable {

    public override init(dependency: SignInDependency) {
        super.init(dependency: dependency)
    }

    open func build(
        withListener listener: SignInListener
    ) -> SignInRouting {
        
        let viewController = UIStoryboard(
            name: "SignIn",
            bundle: nil
        ).instantiateInitialViewController() as! SignInViewController

        let component = SignInComponent(
            dependency: dependency,
            signInViewController: viewController
        )
        
        let interactor = SignInInteractor(
            presenter: viewController
        )
        interactor.listener = listener
        
        return SignInRouter(
            interactor: interactor,
            viewController: viewController
        )
    }
}
