//
//  SignInViewController.swift
//  mvsdkswift
//
//  Created by Igor de Oliveira Sa on 13/11/17.
//  Copyright © 2017 Mobile Vale. All rights reserved.
//

import RIBs
import RxSwift
import UIKit



public protocol SignInPresentableListener: class {
    // TODO: Declare properties and methods that the view controller can invoke to perform
    // business logic, such as signIn(). This protocol is implemented by the corresponding
    // interactor class.
    func didLoad()
    func didCloseTap()
}

open class SignInViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate, SignInPresentable, SignInViewControllable {
    
    /// The UIKit view representation of this view.
    public final var uiviewController: UIViewController { return self }

    public weak var listener: SignInPresentableListener?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction open func onCloseButtonTap(_ sender: Any) {
        self.listener?.didCloseTap()
    }
    
    open func dismiss(viewController: ViewControllable, completion: (() -> Void)?) {
        dismiss(animated: true, completion: completion)
    }
}
